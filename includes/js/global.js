/* 
 *
 * This the global events script of every pages. 
 * Author : Reygie Florida Tarroquin
 * =================================
 * Date : 01|31|2018
 * Time : 03:43 PM
 *
 */
$ = (typeof $ !== 'undefined') ? $ : {};
$.crm = (typeof $.crm !== 'undefined') ? $.crm : {};

$.crm = (function() {

	var __globalEvent = function(){

     var target;

     // .ShowSideBox = event main class
     // data-target = "#ModalTarget"
     // HideSideBox = hidding sidebox

    $('.ShowSideBox').unbind('click').on('click',function(){
         console.log("clicked");
         //get data-target
         target = $(this).data('target');

        $(target).css('width','100%');
        $(target).addClass("col-md-4");
        $(target).show(200);
        $(this).addClass("SideBoxShow");

        $(''+target+' .row').removeClass("hidden");

         console.log(target);

        $('.content-wrapper').off();

        setTimeout(function(){ 

            $('.content-wrapper').unbind('click').on('click',function(){
                $('.ShowSideBox').removeClass("SideBoxShow");
                $(''+target+' .row').addClass("hidden");
                $( target).animate({
                     width: 0,
                  }, 200, function() {
                     $(target).hide();
                     $(target).css('width','100%');
                });
            });
         }, 100);
    });

     $('.HideSideBox').on('click',function(){
      console.log("test");
       $('.ShowSideBox').removeClass("SideBoxShow");
       $(''+target+' .row').addClass("hidden");

           $( target ).animate({
               width: 0,
            }, 200, function() {
              $(target).hide();
              $(target).css('width','100%');
          });
     });


	};



	return {
      globalEvent:__globalEvent,
     
    };
}());